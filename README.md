# To build:
Run cargo build --release in the project directory. Your executable will be in /target/release.

# Usage:
`dae2gltf --input <path_to_msgpack_model>`