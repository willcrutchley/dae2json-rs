use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::PathBuf;

#[derive(Deserialize, Serialize)]
pub struct BoundlessModel {
    pub effects: HashMap<String, ModelEffect>,
    pub geometries: HashMap<String, ModelGeometry>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub images: Option<HashMap<String, PathBuf>>,
    pub materials: HashMap<String, ModelMaterial>,
    pub nodes: HashMap<String, ModelNode>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub skeletons: Option<HashMap<String, ModelSkeleton>>,
    pub version: i64,
}

impl std::fmt::Debug for BoundlessModel {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f, 
            "BoundlessModel:
            {} effect(s),
            {} geometr(y/ies),
            {} material(s),
            {} parent node(s),
            skeleton: {},
            version: {}", 
            self.effects.len(), 
            self.geometries.len(), 
            self.materials.len(), 
            self.nodes.len(), 
            self.skeletons.is_some(), 
            self.version
        )
    }
}

#[derive(Deserialize, Serialize)]
pub struct ModelEffect {
    meta: Option<EffectMeta>,
    parameters: EffectParams,
    #[serde(rename = "type")]
    effect_type: String,
}

#[derive(Deserialize, Serialize)]
pub struct EffectMeta {
    #[serde(skip_serializing_if = "Option::is_none")]
    normals: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    tangents: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    transparent: Option<bool>,
}

#[derive(Deserialize, Serialize)]
pub struct EffectParams {
    #[serde(skip_serializing_if = "Option::is_none")]
    diffuse: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    glow_map: Option<String>,
    #[serde(rename = "materialColor")]
    #[serde(skip_serializing_if = "Option::is_none")]
    material_color: Option<RGBA>,
    #[serde(skip_serializing_if = "Option::is_none")]
    normal_map: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    reflectivity: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    shininess: Option<f64>,
}

#[derive(Deserialize, Serialize)]
pub struct ModelGeometry {
    pub inputs: HashMap<String, GeometryInput>,
    pub meta: GeometryMeta,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub skeleton: Option<String>,
    pub sources: HashMap<String, GeometrySource>,
    pub surfaces: HashMap<String, GeometrySurface>,
}

#[derive(Deserialize, Serialize)]
pub struct GeometryInput {
    pub offset: i64,
    pub source: String,
}

#[derive(Deserialize, Serialize)]
pub struct GeometryMeta {
    #[serde(skip_serializing_if = "Option::is_none")]
    graphics: Option<bool>,
}

#[derive(Deserialize, Serialize)]
pub struct GeometrySource {
    pub data: Vec<f64>,
    pub max: Vec<f64>,
    pub min: Vec<f64>,
    pub stride: i64,
}

#[derive(Deserialize, Serialize)]
pub struct GeometrySurface {
    #[serde(rename = "numPrimitives")]
    pub num_primitives: i64,
    pub triangles: Vec<i64>,
}

#[derive(Deserialize, Serialize)]
pub struct ModelMaterial {
    effect: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    meta: Option<MaterialMeta>,
    #[serde(skip_serializing_if = "Option::is_none")]
    parameters: Option<MaterialParams>, 
}

#[derive(Deserialize, Serialize)]
pub struct MaterialMeta {
    #[serde(skip_serializing_if = "Option::is_none")]
    alpha: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    aux: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    special_material: Option<String>,
}

#[derive(Deserialize, Serialize)]
pub struct MaterialParams {
    #[serde(skip_serializing_if = "Option::is_none")]
    diffuse: Option<PathBuf>,
    #[serde(skip_serializing_if = "Option::is_none")]
    gradient_mask: Option<PathBuf>,
    #[serde(skip_serializing_if = "Option::is_none")]
    normal: Option<PathBuf>,
    #[serde(skip_serializing_if = "Option::is_none")]
    specular_emissive: Option<PathBuf>,
}

#[derive(Deserialize, Serialize)]
pub struct ModelNode {
    #[serde(skip_serializing_if = "Option::is_none")]
    matrix: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    nodes: Option<HashMap<String, ModelNode>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    dynamic: Option<bool>,
    #[serde(rename = "geometryinstances")]
    #[serde(skip_serializing_if = "Option::is_none")]
    geometry_instances: Option<HashMap<String, NodeGeometryInstance>>
}

#[derive(Deserialize, Serialize)]
pub struct NodeGeometryInstance {
    geometry: String,
    material: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    skinning: Option<bool>,
    surface: String
}

#[derive(Deserialize, Serialize)]
pub struct ModelSkeleton {
    #[serde(rename = "bindPoses")]
    bind_poses: Vec<Vec<f64>>,
    #[serde(rename = "invBoneLTMs")]
    inv_bone_ltms: Vec<Vec<f64>>,
    names: Vec<String>,
    #[serde(rename = "numNodes")]
    num_nodes: i64,
    parents: Vec<i64>,
}

#[derive(Deserialize, Serialize)]
pub struct RGBA {
    r: f64,
    g: f64,
    b: f64,
    a: f64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTF {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub accessors: Option<Vec<GLTFAccessor>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub animations: Option<Vec<GLTFAnimation>>,
    pub asset: GLTFAsset,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub buffers: Option<Vec<GLTFBuffer>>,
    #[serde(rename = "bufferViews")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub buffer_views: Option<Vec<GLTFBufferView>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cameras: Option<Vec<GLTFCamera>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(rename = "extensionsUsed")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions_used: Option<Vec<String>>,
    #[serde(rename = "extensionsRequired")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions_required: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub images: Option<Vec<GLTFImage>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub meshes: Option<Vec<GLTFMesh>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub nodes: Option<Vec<GLTFNode>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub samplers: Option<Vec<GLTFSampler>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub scenes: Option<Vec<GLTFScene>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub scene: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub skins: Option<Vec<GLTFSkin>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub textures: Option<Vec<GLTFTexture>>
}

impl Default for GLTF {
    fn default() -> Self {
        GLTF{
            accessors: None,
            animations: None,
            asset: GLTFAsset::default(),
            buffers: None,
            buffer_views: None,
            cameras: None,
            extensions: None,
            extensions_used: None,
            extensions_required: None,
            extras: None,
            images: None,
            meshes: None,
            nodes: None,
            samplers: None,
            scenes: None,
            scene: None,
            skins: None,
            textures: None
        }
    }
}

#[derive(Deserialize, Serialize)]
pub struct GLTFAccessor {
    #[serde(rename = "bufferView")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub buffer_view: Option<i64>,
    #[serde(rename = "byteOffset")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub byte_offset: Option<i64>,
    #[serde(rename = "componentType")]
    pub component_type: i64,
    pub count: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub max: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub min: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub normalized: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sparse: Option<GLTFSparse>,
    pub r#type: String,
    
}

#[derive(Deserialize, Serialize)]
pub struct GLTFAnimation {
    channels: Vec<GLTFChannel>,
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    name: String,
    samplers: Vec<GLTFAnimationSampler>,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFAnimationSampler {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    input: i64,
    interpolation: String,
    output: i64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFAsset {
    #[serde(skip_serializing_if = "Option::is_none")]
    copyright: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    generator: Option<String>,
    #[serde(rename = "minVersion")]
    #[serde(skip_serializing_if = "Option::is_none")]
    min_version: Option<String>,
    version: String,
}

impl Default for GLTFAsset {
    fn default() -> Self {
        GLTFAsset{
            copyright: None,
            extensions: None,
            extras: None,
            generator: None,
            min_version: None,
            version: "2.0".to_owned()
        }
    }
}

#[derive(Deserialize, Serialize)]
pub struct GLTFBuffer {
    #[serde(rename = "byteLength")]
    pub byte_length: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    pub uri: String,
}

#[derive(Deserialize, Serialize, PartialEq)]
pub struct GLTFBufferView {
    pub buffer: i64,
    #[serde(rename = "byteOffset")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub byte_offset: Option<i64>,
    #[serde(rename = "byteLength")]
    pub byte_length: i64,
    #[serde(rename = "byteStride")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub byte_stride: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target: Option<i64>,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFCamera {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    name: String,
    orthographic: GLTFOrthographic,
    perspective: GLTFPerspective,
    r#type: String,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFChannel {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    sampler: i64,
    target: GLTFTarget,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFExtension {
    // Work out what to do here
}

#[derive(Deserialize, Serialize)]
pub struct GLTFImage {
    #[serde(rename = "bufferView")]
    buffer_view: i64,
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    #[serde(rename = "mimeType")]
    mime_type: String,
    name: String,
    uri: String,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFIndices {
    bufferView: i64,
    byteOffset: i64,
    componentType: i64,
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFMaterial {
    alphaCutoff: f64,
    alphaMode: String,
    doubleSided: bool,
    emissiveFactor: Vec<f64>,
    emissiveTexture: GLTFTextureInfo,
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    normalTexture: GLTFNormalTextureInfo,
    occlusionTexture: GLTFOcclusionTextureInfo,
    pbrMetallicRoughness: GLTFPBRMetallicRoughness,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFMesh {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    pub primitives: Vec<GLTFPrimitive>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub weights: Option<Vec<f64>>,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFNode {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub camera: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub children: Option<Vec<i64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub matrix: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mesh: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rotation: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub scale: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub skin: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub translation: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub weights: Option<Vec<f64>>,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFNormalTextureInfo {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    index: i64,
    scale: f64,
    texCoord: i64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFOcclusionTextureInfo {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    index: i64,
    strength: f64,
    texCoord: i64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFOrthographic {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    xmag: f64,
    ymag: f64,
    zfar: f64,
    znear: f64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFPBRMetallicRoughness {
    baseColorFactor: Vec<f64>,
    baseColorTexture: GLTFTextureInfo,
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    metallicFactor: f64,
    metallicRoughnessTexture: GLTFTextureInfo,
    roughnessFactor: f64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFPerspective {
    aspectRatio: f64,
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    yfov: f64,
    zfar: f64,
    znear: f64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFPrimitive {
    pub attributes: std::collections::HashMap<String, usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub indices: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub material: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mode: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub targets: Option<Vec<HashMap<String, serde_json::Value>>>,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFSampler {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    magFilter: i64,
    minFilter: i64,
    name: String,
    wrapS: i64,
    wrapT: i64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFScene {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extensions: Option<HashMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extras: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    pub nodes: Vec<i64>,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFSkin {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    inverseBindMatrices: i64,
    joints: Vec<i64>,
    name: String,
    skeleton: i64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFSparse {
    count: i64,
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    indices: GLTFIndices,
    values: GLTFValues,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFTarget {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    node: i64,
    path: String,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFTexture {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    name: String,
    sampler: i64,
    source: i64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFTextureInfo {
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
    index: i64,
    texCoord: i64,
}

#[derive(Deserialize, Serialize)]
pub struct GLTFValues {
    bufferView: i64,
    byteOffset: i64,
    extensions: HashMap<String, serde_json::Value>,
    extras: serde_json::Value,
}