extern crate rmpv;
extern crate rmp_serde;
extern crate serde_json;
extern crate clap;

use std::io::prelude::{Read};
use std::fs::File;
use serde::Deserialize;

#[derive(Deserialize)]
struct BoundlessJson {
    data: rmpv::Value,
    keys: Vec<String>
}

pub fn msgpack_to_json(path: &str) -> Result<(String), Box<dyn std::error::Error>> {
    let mut file = File::open(path)?;
    let mut buffer: Vec<u8> = Vec::new();
    file.read_to_end(&mut buffer)?;

    let value: BoundlessJson = rmp_serde::from_read(&buffer[..])?;

	// println!("{}", "MsgPack deserialized, fixing keys...");

    let j: serde_json::Value = serde_json::to_value(fix_keys(value.data, value.keys))?;
	// println!("{}", "Prettifying...");
    let json = serde_json::to_string_pretty(&j)?;

	// println!("{}", "Done! Writing to output file");

    Ok(json)
}

fn fix_keys(o: rmpv::Value, k: Vec<String>) -> rmpv::Value {
    if o.is_array() {
		let mut a: Vec<rmpv::Value> = Vec::new();

		for i in o.as_array().unwrap().to_owned() {
			a.push(fix_keys(i, k.clone()));
		}

		rmpv::Value::Array(a)
    }
    else if o.is_map() {
		let mut m: Vec<(rmpv::Value, rmpv::Value)> = Vec::new();

		for i in o.as_map().unwrap().to_owned() {
			let string = &k[i.0.as_i64().unwrap() as usize];

			let key = rmpv::Value::String(<rmpv::Utf8String>::from(string.to_owned()));
			let val = fix_keys(i.1, k.clone());

			m.push((key, val));

		}

        rmpv::Value::Map(m)
    }
    else {
        o
    }
}
