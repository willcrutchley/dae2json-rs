mod structs;
mod msgpack;

use std::convert::TryInto;
use msgpack::msgpack_to_json;
use clap::{App, Arg};
use serde_json::from_str;

use byteorder::{WriteBytesExt, LittleEndian};

use structs::{BoundlessModel, GLTFAccessor, GLTFBuffer, GLTFBufferView, GLTFPrimitive, GLTFNode, GLTFScene, GLTF, GLTFMesh, ModelGeometry, GeometrySurface, GeometryInput};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let app = App::new("Boundless .dae.msgpack to .gltf converter written in Rust!")
		.author("@willcrutchley - crutchley.william@gmail.com")
		.about("Converts model files from Boundless to gltf 2.0")
		.arg(Arg::with_name("input")
			.short("i")
			.long("input")
			.value_name("PATH")
			.help("The path to the input MsgPack file")
			.required(true))
		.arg(Arg::with_name("output")
			.short("o")
			.long("output")
			.value_name("PATH")
			.help("The path to the file to write to")
			.required(false))
		.get_matches();

    let input_path = app.value_of("input").unwrap();
	let output_default = &input_path.replace(".dae.msgpack", ".gltf");
	let output_path = app.value_of("output").unwrap_or(output_default);

    let string_json = msgpack_to_json(input_path)?;
    let deserialised: BoundlessModel = from_str(&string_json)?;

    println!("{:?}", &deserialised);

    let gltf = convert_boundless_model(deserialised)?;
    use std::io::prelude::{Write};
    use std::fs::File;
    let mut file = File::create(output_path).unwrap();
    file.write_fmt(format_args!("{}", serde_json::to_string_pretty(&gltf).unwrap())).unwrap();

    Ok(())
}

fn convert_boundless_model(model: BoundlessModel) -> Result<GLTF, Box<dyn std::error::Error>> {
    let mut gltf = GLTF::default();

    let mut buffers: Vec<GLTFBuffer> = vec!();
    let mut buffer_views: Vec<GLTFBufferView> = vec!();
    let mut meshes: Vec<GLTFMesh> = vec!();
    let mut accessors: Vec<GLTFAccessor> = vec!();
    let mut nodes: Vec<GLTFNode> = vec!();

    for (_, geometry) in model.geometries {
        let (mut geometry_buffers, mut geometry_buffer_views, mut geometry_meshes, mut geometry_accessors) = handle_geometry(&geometry)?;
        buffers.append(&mut geometry_buffers);
        buffer_views.append(&mut geometry_buffer_views);
        meshes.append(&mut geometry_meshes);
        accessors.append(&mut geometry_accessors);
    }

    for i in 0..meshes.len() {
        let node = GLTFNode{
            name: None,
            extras: None,
            extensions: None,
            mesh: Some(i as i64),
            camera: None,
            children: None,
            matrix: None,
            rotation: None,
            scale: None,
            skin: None,
            translation: None,
            weights: None
        };
        nodes.push(node);
    }

    let mut scene_nodes: Vec<i64> = vec!();
    for i in 0..meshes.len() {
        scene_nodes.push(i as i64);
    }

    gltf.accessors = Some(accessors);
    gltf.buffers = Some(buffers);
    gltf.buffer_views = Some(buffer_views);
    gltf.meshes = Some(meshes);
    gltf.nodes = Some(nodes);
    gltf.scene = Some(0);
    gltf.scenes = Some(vec!(GLTFScene{
        extensions: None,
        extras: None,
        name: None,
        nodes: scene_nodes,
    }));

    Ok(gltf)
}

fn handle_geometry(geometry: &ModelGeometry) -> Result<(Vec<GLTFBuffer>, Vec<GLTFBufferView>, Vec<GLTFMesh>, Vec<GLTFAccessor>), Box<dyn std::error::Error>> {
    let surfaces: Vec<&GeometrySurface> = geometry.surfaces.values().collect();

    let mut geometry_buffers: Vec<GLTFBuffer> = vec!();
    let mut geometry_buffer_views: Vec<GLTFBufferView> = vec!();

    let mut meshes: Vec<GLTFMesh> = vec!();
    let mut accessors: Vec<GLTFAccessor> = vec!();

    for (surface_counter, surface) in surfaces.iter().enumerate() {
        let (buffer, mut buffer_views, mesh, mut surface_accessors) = handle_surface(&surface, &geometry, surface_counter.try_into().unwrap())?;
        geometry_buffers.push(buffer);
        geometry_buffer_views.append(&mut buffer_views);
        meshes.push(mesh);
        accessors.append(&mut surface_accessors);
    }

    Ok((geometry_buffers, geometry_buffer_views, meshes, accessors))
}
// Matrices are column-major, we have the top 3 values in each column, the bottom row is always the same
fn handle_surface(surface: &GeometrySurface, parent_geometry: &ModelGeometry, counter: i64) -> Result<(GLTFBuffer, Vec<GLTFBufferView>, GLTFMesh, Vec<GLTFAccessor>), Box<std::error::Error>> {
    let mut inputs: Vec<&GeometryInput> = parent_geometry.inputs.values().collect();
    let max_offset = get_max_offset(&inputs);

    let mut surface_buffer_data: Vec<u8> = vec!();
    let mut surface_buffer_views: Vec<GLTFBufferView> = vec!();

    let mut surface_buffer_length_counter: i64 = 0;

    let indices: &Vec<i64> = &surface.triangles;
    let mut indices_offsets: Vec<Vec<usize>> = vec!(vec!(); max_offset + 1);

    let mut accessors: Vec<GLTFAccessor> = vec!();

    for i in 0..=max_offset {
        for j in indices.chunks(max_offset + 1) {
            indices_offsets[i].push(j[i] as usize);
        }
    }

    let mut b_v_counter = 0;
    let mut temp_inputs: Vec<&GeometryInput> = vec!();
    for i in &inputs {
        if !i.source.contains("binormal") {
            temp_inputs.push(i);
        }
    }
    inputs = temp_inputs;

    for &input in &inputs {
        let (mut buffer_data, buffer_view, accessor) = handle_input(input, &indices_offsets, parent_geometry, counter, surface_buffer_length_counter, b_v_counter, inputs.len());
        surface_buffer_length_counter += buffer_data.len() as i64;
        surface_buffer_data.append(&mut buffer_data);
        surface_buffer_views.push(buffer_view);
        b_v_counter += 1;
        accessors.push(accessor);
    }

    let mut attributes: std::collections::HashMap<String, usize> = std::collections::HashMap::new();
    for i in 0..surface_buffer_views.len() {
        let s_b_v = &surface_buffer_views[i];
        let name = s_b_v.name.clone().unwrap();
        if name.contains("position") {
            attributes.insert("POSITION".to_string(), i + (counter as usize * surface_buffer_views.len()));
        }
        else if name.contains("normal") {
            attributes.insert("NORMAL".to_string(), i + (counter as usize * surface_buffer_views.len()));
        }
        else if name.contains("map1") {
            attributes.insert("TEXCOORD_0".to_string(), i + (counter as usize * surface_buffer_views.len()));
        }
        else if name.contains("tangent") {
            attributes.insert("TANGENT".to_string(), i + (counter as usize * surface_buffer_views.len()));
        }
    }

    let buffer = GLTFBuffer{
        name: None,
        extensions: None,
        extras: None,
        byte_length: surface_buffer_data.len(),
        uri: format!("data:application/octet-stream;base64,{}", base64::encode(&surface_buffer_data))
    };

    let primitive = GLTFPrimitive {
        extensions: None,
        extras: None,
        attributes,
        indices: None,
        material: None,
        mode: None,
        targets: None
    };

    let mesh = GLTFMesh {
        extensions: None,
        extras: None,
        weights: None,
        name: None,
        primitives: vec!(primitive)
    };
    
    Ok((buffer, surface_buffer_views, mesh, accessors))
}

fn get_max_offset(inputs: &[&GeometryInput]) -> usize {
    let mut max: usize = 0;
    for input in inputs {
        if input.offset as usize > max {
            max = input.offset as usize
        }
    }
    max
}

fn handle_input(input: &GeometryInput, indices_offsets: &[Vec<usize>], parent_geometry: &ModelGeometry, counter: i64, offset: i64, input_counter: i64, num_inputs: usize) -> (Vec<u8>, GLTFBufferView, GLTFAccessor) {
    let mut source_data = vec!();
    let mut selected_data = vec!();
    let mut buffer_data: Vec<u8> = vec!();
    let mut stride: i64 = 0;

    for (source_name, source) in &parent_geometry.sources {
        if source_name == &input.source {
            source_data = source.data.clone().chunks(source.stride as usize)
            .map(|i| 
                i.to_vec()
            ).collect();
            stride = source.max.len() as i64;
        }
    }

    for &i in &indices_offsets[input.offset as usize] {
        selected_data.push(&source_data[i]);
    }

    for &i in &selected_data {
        if input.source.contains("tangent") {
            stride = 4;
            for &j in i {
                let mut wtr = vec!();
                wtr.write_f32::<LittleEndian>(j as f32).unwrap();
                buffer_data.append(&mut wtr);
            }
            let mut wtr = vec!();
            wtr.write_f32::<LittleEndian>(-1.0).unwrap();
            buffer_data.append(&mut wtr);
        }
        else {
            for &j in i {
                let mut wtr = vec!();
                wtr.write_f32::<LittleEndian>(j as f32).unwrap();
                buffer_data.append(&mut wtr);
            }
        }
    }

    let buffer_view = GLTFBufferView{
        buffer: counter,
        byte_offset: Some(offset),
        byte_stride: None,
        extensions: None,
        extras: None,
        name: Some((&input.source).to_string()),
        target: None,
        byte_length: selected_data.len() as i64 * 4 * stride as i64
    };

    let accessor = GLTFAccessor{
        extensions: None,
        extras: None,
        byte_offset: None,
        component_type: 5126,
        count: selected_data.len() as i64,
        max: Some(vec!(0.0; stride as usize)),
        min: Some(vec!(0.0; stride as usize)),
        normalized: None,
        name: Some((&input.source).to_string()),
        sparse: None,
        r#type: format!("VEC{}", stride),
        buffer_view: Some(input_counter + (counter * (num_inputs) as i64))
    };

    (buffer_data, buffer_view, accessor)
}
